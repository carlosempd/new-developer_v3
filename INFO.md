## Info about OS and experience

I use macOS on a daily basis and the language I master is javascript and typescript. I have experience with java and python but in the last few years I've only been workin with js and ts, using frameworks like Angular, NestJs and raw NodeJS. 

I use RTFM everyday since as a developer I always encounter myself in situations where something is broken or need to be fixed or doesn't work as it should. Over the years I've learned that maybe it is faster to ask someone to solve the problem quickly, but the learning process requires you to look for a solution on your own, it'll probably take you more time, but you will remember the steps you had to follow in order to get things done.

