# Welcome to RebajaTusCuentas.com

Please complete this guide by uploading your work to your own gitlab repository 
and doing a MR to this one. The branch must contain the readme file with the
responses using markdown and referencing to folders or files
that you need in order to solve the test.


## 1

We encourage documentation and investigation in order to have agile development.
We support RTFM and LMGTFY:

>___Create a file telling us when you last used RTFM and LMGTFY,
the OS you use and the languages you master___

[Here the answer](INFO.md)

## 2

Automation helps us to avoid human errors. Some of our systems use CI.

>___Write a program in the language of your choice that can accomplish this.
You can use Pseudocode.___
>___If you are not familiar with writing these programs, you can explain the
most representative concepts.___

```
# Espera a que se realicen cambios en el repositorio
while esperar_cambios():

    # Descargar el código actualizado del repositorio
    codigo = descargar_codigo()

    # Se ejecutan las pruebas unitarias en el código descargado
    exito_pruebas = ejecutar_pruebas_unitarias(codigo)

    # Si las pruebas unitarias fallan, se detiene la integración continua y se envía una notificación
    if not exito_pruebas:
        detener_integracion_continua()
        enviar_notificacion('Fallaron las pruebas unitarias')

    # Si las pruebas unitarias pasan, se continúa con la integración continua
    else:
        # Se realiza la compilación y la construcción del proyecto
        exito_compilacion = compilar_proyecto(codigo)

        # Si la compilación falla, se detiene la integración continua y se envía una notificación
        if not exito_compilacion:
            detener_integracion_continua()
            enviar_notificacion('Falló la compilación')

        # Si la compilación es exitosa, continúa con la integración continua
        else:
            # Implementa los cambios en el servidor de prueba
            exito_despliegue = desplegar_servidor_prueba()

            # Si el despliegue falla, se detiene la integración continua y envía una notificación
            if not exito_despliegue:
                detener_integracion_continua()
                enviar_notificacion('Falló el despliegue')

            # Si el despliegue es exitoso, continúa con la integración continua
            else:
                # Si se ha llegado hasta aquí, la integración continua se ha realizado con éxito
                enviar_notificacion('Integración continua exitosa')
```


## 3


A developer's portfolio is important to us. We ask you to upload 1 or 2 
projects of your authorship.

>___If you do not want to share the code, you can just paste some of it.___


## 4

>___Please, write a code or pseudocode that solves the problem in which I have a 
collection of numbers in ascending order. You need to find the matching pair 
that it is equal to a sum that its also given to you. If you make any 
assumption, let us know.___

>___Example:___
* [2,3,6,7]  sum = 9  - OK, matching pair (3,6)
* [1,3,3,7]  sum = 9  - No

* Consider recibe 1 000 000 numbers

El procediminento seria algo como sigue:

1. Establecer dos punteros, left y right, al principio y al final del arreglo, respectivamente.
2. Mientras left < right:
     1. Calcular la suma de los elementos en las posiciones left y right.
     2. Si la suma es igual al valor objetivo, retornar el par de elementos.
     3. Si la suma es menor que el valor objetivo, aumentar left en 1.
     4. Si la suma es mayor que el valor objetivo, disminuir right en 1.
3. Si no se encuentra ningún par que sume al valor objetivo, retornar un mensaje indicando que no se encontró ningún par.


Para optimizar la búsqueda, se pudiera implementar un binary search, aqui un ejemplo en javascript:


```
function binarySearch(arr, left, right, target) {
  while (left <= right) {
    let mid = left + Math.floor((right - left) / 2);
    if (arr[mid] == target) {
      return mid;
    } else if (arr[mid] < target) {
      left = mid + 1;
    } else {
      right = mid - 1;
    }
  }
  return -1;
}

function findPair(arr, sum) {
  let n = arr.length;
  for (let i = 0; i < n - 1; i++) {
    let j = binarySearch(arr, i + 1, n - 1, sum - arr[i]);
    if (j != -1) {
      return [arr[i], arr[j]];
    }
  }
  return null;
}

// Ejemplo de uso
let arr = [1, 2, 3, 4, 5, 6];
let sum = 7;
let result = findPair(arr, sum);
console.log(result); // output: [1, 6]



```

## 5

"The message is in spanish."

>___4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772
6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c
736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374
612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220
617175ed212e___


>___U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVu
IG1lbnNhamUu___



# All answers must be inside a docker image and the answer will be tested with a running container. Add lint and at least 01 unit test
